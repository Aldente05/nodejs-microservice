/**
 * Created by f.putra on 2019-05-02.
 * controller
 */
let express = require('express');
let userRouter = express.Router();
let userService = require('../services/UserServices')
let {authorize, utils} = require('./../_helpers')

userRouter.get('/hello', (req, res, next) => {
    /* 	#swagger.tags = ['Hello']
        #swagger.description = 'hello controller' */
    utils.doResponse(res, 200, "HELLO")
})
userRouter.post('/register', (req, res, next) => {
    /* 	#swagger.tags = ['User']
        #swagger.description = 'Register User Controller' */

    /* #swagger.parameters['Register'] = {
              in: 'body',
              description: 'register user',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/Register" }
       } */
    userService.register(req.body).then(result => {
        utils.doResponse(res, 200, result);
    }).catch(() => {
        utils.doResponse(res, 403, "Your Identity has exist");
    })
})

userRouter.post('/login', (req, res, next) => {
    /* 	#swagger.tags = ['User']
        #swagger.description = 'Login User Controller' */

    /* #swagger.parameters['Login'] = {
              in: 'body',
              description: 'login user',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/Login" }
       } */
    userService.login(req.body).then(result => {
        utils.doResponse(res, result.code, result.message);
    })
})

userRouter.patch('/update', authorize.authenticate, (req, res, next) => {
    /* 	#swagger.tags = ['User']
        #swagger.description = 'Update User Controller' */

    /* #swagger.parameters['Update'] = {
              in: 'body',
              description: 'Update User Controller',
              required: true,
              type: 'object',
              schema: { $ref: "#/definitions/UpdateUser" }
       } */
    /* #swagger.security = [{
            "JWT": []
    }] */
    userService.updateProfile(req.body).then(() => {
        utils.doResponse(res, 200, "Update Success");
    }).catch(next)
})

userRouter.get('/list', authorize.authenticate, (req, res) => {
    /* 	#swagger.tags = ['User']
        #swagger.description = 'Get All User Controller'
        #swagger.parameters['offset'] = { description: 'offset' }
        #swagger.parameters['limit'] = { description: 'limit' } */
    /* #swagger.security = [{
            "JWT": []
    }] */
    userService.getAll(req.query.offset, req.query.limit).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(err => {
        utils.doResponse(res, 400, err)
    })
})

userRouter.get('/findByAccountNumber', authorize.authenticate, (req, res) => {
    /* 	#swagger.tags = ['User']
        #swagger.description = 'Get User By Account Number Controller'
        #swagger.parameters['accNumber'] = { description: 'Account Number' } */
    /*  #swagger.parameters['accNumber'] = {
               description: 'Account Number',
               in:'query',
               type: 'string'
        } */
    /* #swagger.security = [{
            "JWT": []
    }] */
    userService.findByAccountNumber(req.query.accNumber).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(err => {
        utils.doResponse(res, 400, err)
    })
})

userRouter.get('/findByIdentityNumber', authorize.authenticate, (req, res) => {
    /* 	#swagger.tags = ['User']
        #swagger.description = 'Get User By Identity Number Controller'
        #swagger.parameters['idnNumber'] = { description: 'idnNumber' } */
    /*  #swagger.parameters['idnNumber'] = {
               description: 'Identity Number',
               in:'query',
               type: 'string'
        } */
    /* #swagger.security = [{
            "JWT": []
    }] */
    userService.findByIdentityNumber(req.query.idnNumber).then(result => {
        utils.doResponse(res, 200, result)
    }).catch(err => {
        utils.doResponse(res, 400, err)
    })
})

module.exports = userRouter;
