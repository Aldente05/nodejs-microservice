/**
 * @author fputra on 07/02/22
 */
let sequelize = require('../RepositoryConfig');
const Sequelize = require('sequelize');

const organisation_unit = sequelize.define('org', {
    id :{
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey : true
    },
    ParentObjectAlias:{type: Sequelize.STRING },
    ParentObjectName:{type: Sequelize.STRING },
    PropertyValue:{type: Sequelize.STRING },
    OUtype:{type: Sequelize.STRING },
});
module.exports = organisation_unit;
