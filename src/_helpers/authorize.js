/**
 * Created by f.putra on 2019-04-02.
 * json web token creator for create token session
 */
let jwt = require('jsonwebtoken');
let { utils } = require('./../_helpers');

let authorize = {
    authenticate : (req, res, next) => {
        const authHeader = req.headers.authorization;
        if (authHeader) {
            const token = authHeader.split(' ')[1];

            jwt.verify(token, JSON.parse(process.env.JWT_CONFIG).secret, (err, user) => {
                if (err) {
                    utils.doResponse(res, 403, err);
                }

                req.user = user;
                next();
            });
        } else {
            utils.doResponse(res, 401, 'Unauthorized');
        }
    }
}

module.exports = authorize;

