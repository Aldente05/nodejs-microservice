/**
 * Created by f.putra on 2019-04-02.
 * json web token configuration url
 */
const expressJwt = require('express-jwt');

function jwt() {
    return expressJwt(JSON.parse(process.env.JWT_CONFIG)).unless({
        path: [
            new RegExp('/.*', 'i'),
            // '/route/v1/user/hello',
            // '/route/v1/user/login',
            // '/route/v1/user/register',
            // new RegExp('/route/v1/user.*', 'i'),
            // public routes that don't require authentication
            // new RegExp('/.*', 'i'),
            // new RegExp('/export.*', 'i'),
        ]
    });
}

module.exports = jwt;
