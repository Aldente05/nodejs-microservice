FROM node:12.13.0-alpine
WORKDIR /app
RUN mkdir /app/logs/
# Bundle APP files
COPY . /app

# Install app dependencies
ENV NPM_CONFIG_LOGLEVEL warn
RUN npm install
RUN npm i -g pm2
# Expose the listening port of your app
EXPOSE 3200
CMD pm2 start ecosystem.config.js --env production  && tail -f /dev/null
