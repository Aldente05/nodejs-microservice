# NODEJS RESTAPI MICROSERVICES

# requirement ENV
- nodejs
- pm2
- docker redis / your local postgresql
- docker mongodb / your local mongodb
- yarn

```
├── README.md
├── Dockerfile
├── docker-compose.yml
├── app.js -> main file configuration express
├── swagger.js -> main file start and swagger config
├── swagger_output.json -> automaticaly generated from swagger-autogen
├── logs -> logging folder
├── ecosystem.config.js -> environment config
├── src
    ├── _helpers
    ├── controllers
    ├── core
    └── services
```

clone this repo and install all dependencies on root folder project
- `yarn install`

running on development open localhost:3200
- `yarn start`

runniong on production
- `yarn deploy`
